//Name: Dan Willis Njoumene Douanla       ID: 2033804
public class BikeStore{
    public static void main(String[] args){
        
        Bicycle  store[] = new Bicycle[4];
        store[0] = new Bicycle("Lynskey", 4, 100);
        store[1] = new Bicycle("REEB", 5, 200);
        store[2] = new Bicycle("Moots", 6, 300);
        store[3] = new Bicycle("Allied", 7, 400);

        for (Bicycle bike: store){
            System.out.println(bike);
        }
    }
}
